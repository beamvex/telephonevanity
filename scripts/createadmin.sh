#!/bin/bash

#aws connect list-routing-profiles --instance-id $2

#aws connect list-security-profiles --instance-id $2

aws connect create-user --username admin --password $1 --instance-id $2 \
    --phone-config PhoneType=SOFT_PHONE,AutoAccept=false \
    --routing-profile-id "23cdf792-3ee6-4375-bcef-4b2e26031171" \
    --security-profile-ids  "320362e3-1646-4fb8-b8ad-f9baac0cdcdd" \
    --identity-info FirstName=Robert,LastName=Forster