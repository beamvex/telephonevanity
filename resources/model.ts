export interface vanityRecord {
  telephoneNumber: string;
  lastUpdate: number;
  vanityNumbers: string[];
}
