import type { Handler, Context, ConnectContactFlowEvent } from "aws-lambda";
import { saveData } from "./database";
import type { vanityRecord } from "./model";
import { convertTelephoneNumber } from "./convertor";
import type { vanityNumber } from "./convertor";

/**
 * Lambda to handle telephone numbers
 * from connect app
 * convert to 5 best vanity
 * and store in DynamoDB
 *
 * @param {ConnectContactFlowEvent} event event data
 * @param {Context} context context of request
 */
export const handler: Handler = async (
  event: ConnectContactFlowEvent,
  context: Context
) => {
  const caller = event.Details.ContactData.CustomerEndpoint?.Address ?? "";

  const topnumbers: string[] = [];

  console.debug(caller);

  if (caller !== "") {
    const vanityNumbers = convertTelephoneNumber(caller);

    vanityNumbers.forEach((vanity: vanityNumber) => {
      topnumbers.push(vanity.vanityNumber);
    });

    const data: vanityRecord = {
      telephoneNumber: caller,
      lastUpdate: new Date().getTime(),
      vanityNumbers: topnumbers,
    };

    const saved = await saveData(caller, data);
    if (saved) {
      console.debug("saved number");
    }
  }

  const top3: string[] = [];

  for (let i = 0; i < 3; i++) {
    top3.push(topnumbers[i].split("").join(" "));
  }

  return {
    caller: caller.split("").join(" "),
    top1: top3[0],
    top2: top3[1],
    top3: top3[2],
  };
};
