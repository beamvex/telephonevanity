/**
 * Regex to extract number without area code
 */
const TELEPHONE_REGEX = /^\+44[0-9]{4}([0-9]{6})$/;

/**
 * Extract number without area code from number
 *
 * @param telephone number with area code
 * @returns number without area code
 */
export function extractNumber(telephone: string): string {
  const matches = telephone.match(TELEPHONE_REGEX);
  if (matches !== null) {
    return matches[1];
  } else {
    throw new Error("Telephone number is invalid");
  }
}
