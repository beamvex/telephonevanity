import * as fs from "fs";

/**
 * Load words into memory
 */
const WORDLIST = fs.readFileSync("resources/wordlist.csv");

export function isWord(word: string): boolean {
  return WORDLIST.indexOf(word) >= 0;
}
