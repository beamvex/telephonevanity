import * as AWS from "aws-sdk";
import type { vanityRecord } from "../model";

const TABLE_NAME = process.env.TABLE_NAME ?? "";

const db = new AWS.DynamoDB.DocumentClient();

/**
 * Persist telephone number data
 *
 * @param telephone number with area code
 * @returns number without area code
 */
export async function saveData(
  telephone: string,
  data: vanityRecord
): Promise<boolean> {
  const item: vanityRecord = {
    ...data,
    lastUpdate: new Date().getTime(),
    telephoneNumber: telephone,
  };

  const params = {
    TableName: TABLE_NAME,
    Item: item,
  };

  try {
    await db.put(params).promise();
    return true;
  } catch (dbError) {
    // TODO add some error handling
    return false;
  }
}
