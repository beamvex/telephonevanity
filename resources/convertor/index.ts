import { calcPermulations } from "../calcperm";
import { extractNumber } from "../validator";
import { isWord } from "../wordchecker";

export interface vanityNumber {
  wordToTest: string;
  wordSub: string;
  wordToFind: string;
  numberWithoutArea: string;
  vanityNumber: string;
  score: number;
}

export type vanityNumbers = Record<string, vanityNumber>;

/**
 * Convert telephone number to 5 best vanity numbers
 *
 * @param {string} telephone telephone number to convert
 * @returns 5 best vanity numbers for supplied number
 */
export const convertTelephoneNumber = (telephone: string): vanityNumber[] => {
  const result: vanityNumbers = {};

  let topfive: vanityNumber[] = [];

  const numberWithoutArea = extractNumber(telephone);

  console.debug("extracted number", numberWithoutArea);

  // calc all permutations
  const perms = calcPermulations(numberWithoutArea);

  // find a word
  perms.forEach((word: string) => {
    const wordToTest = word.toLowerCase();
    const wordsWithNumberSubs = substituteNumbers(wordToTest).concat([
      wordToTest,
    ]);

    wordsWithNumberSubs.forEach((wordSub: string) => {
      let found = false;

      for (let i = 6; i >= 4; i--) {
        const wordToFind = wordSub.substring(0, i);
        if (isWord(wordToFind)) {
          const vanityNumber =
            wordToTest.substring(0, i) +
            "-" +
            numberWithoutArea.substring(i, numberWithoutArea.length);

          found = true;

          result[wordToFind] = {
            wordToTest,
            wordSub,
            wordToFind,
            numberWithoutArea,
            vanityNumber,
            score: i,
          };
          break;
        }
      }

      if (!found) {
        result[wordToTest] = {
          wordToTest,
          wordSub,
          numberWithoutArea,
          vanityNumber: wordToTest,
          score: 0,
          wordToFind: "",
        };
      }
    });
  });

  // add all to array
  Object.keys(result).forEach((key) => {
    topfive.push(result[key]);
  });

  // sort
  topfive = topfive.sort((a: vanityNumber, b: vanityNumber) => {
    if (a.score === b.score) return 0;
    if (a.score > b.score) return -1;
    return 1;
  });

  return topfive.slice(0, 5);
};

type keyNumbersType = Record<string, string[]>;

const NUMBER_SUBS: keyNumbersType = {
  "0": ["o"],
  "1": ["i"],
};

export function substituteNumbers(
  wordToTest: string,
  index = 0,
  vanityNumber = "",
  resultArr = Array<string>()
): string[] {
  const digit = wordToTest.charAt(index);

  const replacements = NUMBER_SUBS[digit] ?? [digit];

  for (let i = 0; i < replacements.length; i++) {
    if (index < wordToTest.length - 1) {
      substituteNumbers(
        wordToTest,
        index + 1,
        vanityNumber + replacements[i],
        resultArr
      );
    } else {
      resultArr.push(vanityNumber + replacements[i]);
    }
  }

  return resultArr;
}

export function calcSubVariants(wordSub: string): string[] {
  const result: string[] = [];

  for (let i = 4; i < 6; i++) {
    const variants = calcSubVariantsBySize(wordSub, i);
    variants.forEach((variant: string) => {
      result.push(variant);
    });
  }

  return result;
}

export function calcSubVariantsBySize(wordSub: string, size: number): string[] {
  const result: string[] = [];

  for (let i = 0; i < wordSub.length; i++) {
    if (i + size < wordSub.length + 1) {
      const variant = wordSub.substring(i, size + i);
      result.push(variant);
    }
  }

  return result;
}
