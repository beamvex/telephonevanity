type keyNumbersType = Record<string, string[]>;

const KEY_NUMBERS: keyNumbersType = {
  "2": ["A", "B", "C"],
  "3": ["D", "E", "F"],
  "4": ["G", "H", "I"],
  "5": ["J", "K", "L"],
  "6": ["M", "N", "O"],
  "7": ["P", "Q", "R", "S"],
  "8": ["T", "U", "V"],
  "9": ["W", "X", "Y", "Z"],
};

/**
 * Calculate permutations
 * recusive function for each digit
 * find all the permutations for each digit
 * add to array
 *
 * @param {string} number to calc permuations
 * @returns all permuations of possible vanity numbers
 */
export const calcPermulations = (
  number: string,
  index = 0,
  vanityNumber = "",
  resultArr = Array<string>()
): string[] => {
  const digit = number.charAt(index);

  const replacements = KEY_NUMBERS[digit] ?? [digit];

  for (let i = 0; i < replacements.length; i++) {
    if (index < number.length - 1) {
      calcPermulations(
        number,
        index + 1,
        vanityNumber + replacements[i],
        resultArr
      );
    } else {
      resultArr.push(vanityNumber + replacements[i]);
    }
  }

  return resultArr;
};
