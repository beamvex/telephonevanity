# Welcome to the Telephone Vanity project

## About

This project purpose is to integrate with AWS Connect with a lambda
which converts telephone numbers into vanity numbers
it assumes numbers are in the UK format e.g. +441522431894
it will generate vanitiy numbers from the last 6 digits on the number
to build a word or phrase 6 letters in total.

In vanity numbers The letters of the word or phrase relates to digits on
the telephone keypad as follows.

```
+-------+-------+-------+
|   1   |   2   |   3   |
|       |  ABC  |  DEF  |
+-------+-------+-------+
|   4   |   5   |   6   |
|  GHI  |  JKL  |  MNO  |
+-------+-------+-------+
|   7   |   8   |   9   |
|  PQRS |  TUV  |  WXYZ |
+-------+-------+-------+
|   *   |   0   |   #   |
|       |       |       |
+-------+-------+-------+
```

The first part of the algorythm is to extract last 6 digits while checking
it complies with a regex

then calculating all the permuations with converting numbers to key pad digits.

to find the best choices then i will see if the generate "vanity" numbers exist
in a 6 letter word list. I will capitalise that some numbers can also be read as letters as follows:

0 == O
1 == I or L
3 == E
4 == A
5 == S
7 == T
8 == B

###### Author: Robert Forster

## build process

I have attempted to make a build chain in package json such that build
and deploy requires linting and unit tests to run first

If i had more time i would enforce this furher so that it would be impractical to
bypass and use commit hooks to prevent code which fails tests/liniting into git repo

## lint

im using eslint with the prettier rules as i find this to be the best fit for my style

## Jest

im using jest for unit test; configured to produce coverage reports and enforce 90% coverage.
coverage is not the best metric and with more time i would chose other methods to ensure quality.

Also with more time i would remember the setting in jest which controls recording coverage on files
with no tests.

## esbuild

Im using esbuild to bundle the code including dependencies into single js
file for lambda code

I find this simpler than using the standard aws cdk budndling which relys on docker
and seems to be very slow at bundling: especially for multiple lambdas

i did find that including the aws-sdk in the bundle caused me to raise the mem setting on
the lambda to require 512m. not sure if this is the bundle or just needing the sdk.

## CDK

This project has been created by CDK development with TypeScript.

The `cdk.json` file tells the CDK Toolkit how to execute your app.

Im using CDK as i prefer it to SAM; CDK is closer to the infra-as-code
paradigm and it makes it easier to test with jest

## Useful commands

- `npm run build` compile typescript to js
- `npm run watch` watch for changes and compile
- `npm run test` perform the jest unit tests
- `npx cdk deploy` deploy this stack to your default AWS account/region
- `npx cdk diff` compare deployed stack with current state
- `npx cdk synth` emits the synthesized CloudFormation template
