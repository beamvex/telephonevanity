import * as cdk from "aws-cdk-lib";
import type { Construct } from "constructs";
import { TelephoneService } from "./telephoneservice";
import { Connect } from "./connect";
import { DynamoDB } from "./dynamodb";

/**
 * CDS Stack for Telephone Vanity Stack
 */
export class CdkStack extends cdk.Stack {
  dynampDB: DynamoDB;
  connect: Connect;
  telephoneService: TelephoneService;
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Add DynamoDB
    this.dynampDB = new DynamoDB(this, "VanityTable");

    // Add Telephone lambda service
    this.telephoneService = new TelephoneService(
      this,
      "Telephone",
      this.dynampDB
    );

    // Add AWS Connect to stack
    this.connect = new Connect(this, "Connect", this.telephoneService);
  }
}
