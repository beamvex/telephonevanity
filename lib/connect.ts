import { Construct } from "constructs";
import * as connect from "aws-cdk-lib/aws-connect";
import * as fs from "fs";
import type { TelephoneService } from "./telephoneservice";

export class Connect extends Construct {
  constructor(
    scope: Construct,
    id: string,
    telephoneService: TelephoneService
  ) {
    super(scope, id);

    /**
     * Create Connect Instance for the Vanity telephone service
     */
    const connectInstance = new connect.CfnInstance(scope, "VanityConnect", {
      instanceAlias: "VanityConnect",
      attributes: {
        autoResolveBestVoices: true,
        contactflowLogs: true,
        contactLens: true,
        inboundCalls: true,
        outboundCalls: false,
      },
      identityManagementType: "CONNECT_MANAGED",
    });

    /**
     * Create an inboand phone number for the telephony
     */
    // eslint-disable-next-line no-new
    new connect.CfnPhoneNumber(scope, "VanityInboundPhoneNumber", {
      targetArn: connectInstance.attrArn,
      countryCode: "GB",
      type: "DID",
    });

    let flowContent = fs.readFileSync("resources/flows/answerflow.json", {
      encoding: "utf8",
      flag: "r",
    });

    // arn:aws:lambda:eu-west-2:512752756525:function:CdkStack-TelephoneTelephoneHandler58CA165F-RHuOZjzSWisd
    flowContent = flowContent.replace(
      "FUNCTION_ARN",
      telephoneService.handler.functionArn
    );

    /**
     * Contact flow for answering the phone
     */
    // eslint-disable-next-line no-new
    new connect.CfnContactFlow(scope, "VanityFlow", {
      instanceArn: connectInstance.attrArn,
      name: "VanityFlow",
      content: flowContent,
      type: "CONTACT_FLOW",
    });
  }
}
