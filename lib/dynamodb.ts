import { Construct } from "constructs";
import * as dynamodb from "aws-cdk-lib/aws-dynamodb";
import { AttributeType } from "aws-cdk-lib/aws-dynamodb";
import { RemovalPolicy } from "aws-cdk-lib/core";

export class DynamoDB extends Construct {
  database: dynamodb.TableV2;
  constructor(scope: Construct, id: string) {
    super(scope, id);

    this.database = new dynamodb.TableV2(scope, "VanityNumbers", {
      partitionKey: {
        name: "telephoneNumber",
        type: AttributeType.STRING,
      },
      tableName: "VanityNumbers",

      removalPolicy: RemovalPolicy.DESTROY,
    });
  }
}
