import { Construct } from "constructs";
import * as apigateway from "aws-cdk-lib/aws-apigateway";
import * as lambda from "aws-cdk-lib/aws-lambda";
import type { DynamoDB } from "./dynamodb";

export class TelephoneService extends Construct {
  handler: lambda.Function;
  constructor(scope: Construct, id: string, db: DynamoDB) {
    super(scope, id);

    this.handler = new lambda.Function(this, "TelephoneHandler", {
      runtime: lambda.Runtime.NODEJS_20_X,
      code: lambda.Code.fromAsset("dist"),
      handler: "telephone.handler",
      memorySize: 512,
      environment: {
        PRIMARY_KEY: "telephoneNumber",
        TABLE_NAME: db.database.tableName,
      },
    });

    db.database.grantFullAccess(this.handler);

    const api = new apigateway.RestApi(this, "telephone-api", {
      restApiName: "Telephone Service",
      description:
        "This service generates Vanity numbers for telephone number.",
    });

    const getTelephoneIntegration = new apigateway.LambdaIntegration(
      this.handler,
      {
        requestTemplates: { "application/json": '{ "statusCode": "200" }' },
      }
    );

    api.root.addMethod("GET", getTelephoneIntegration); // GET /
  }
}
