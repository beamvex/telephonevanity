import { extractNumber } from "../../resources/validator";

const TEST_TELEPHONE_NUMBER = "+441782715308";
const TEST_INVALID_TELEPHONE_NUMBER = "01782x715308";
const EXTRACTED_NUMBER = "715308";

test("Telephone Number Validator - Valid Number", async () => {
  const response = extractNumber(TEST_TELEPHONE_NUMBER);

  expect(response).toEqual(EXTRACTED_NUMBER);
});

test("Telephone Number Validator - Invalid Number", async () => {
  expect(() => extractNumber(TEST_INVALID_TELEPHONE_NUMBER)).toThrow(Error);
});
