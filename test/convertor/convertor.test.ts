import {
  calcSubVariants,
  calcSubVariantsBySize,
  convertTelephoneNumber,
} from "../../resources/convertor";
// import { TEST_NUMBERS } from "../testnumbers";

const TEST_TELEPHONE_NUMBER = "+441782715308";
const TEST_WORD = "robert";
const TEST_INVALID_TELEPHONE_NUMBER = "01782x715308";

test("Telephone Number Convertor - Valid Number", async () => {
  const response = convertTelephoneNumber(TEST_TELEPHONE_NUMBER);

  expect(response).toBeDefined();

  console.debug(JSON.stringify(response, null, 2));
});
/*
test("Telephone Number Convertor - Bulk Test Numbers", async () => {
  TEST_NUMBERS.forEach((number) => {
    const response = convertTelephoneNumber(number);

    expect(response).toBeDefined();
    console.debug(JSON.stringify(response, null, 2));
  });
});
*/
test("Telephone Number Convertor - Invalid Number", async () => {
  expect(() => convertTelephoneNumber(TEST_INVALID_TELEPHONE_NUMBER)).toThrow(
    Error
  );
});

test("Telephone Number Convertor - calculate sub variants", async () => {
  const response = calcSubVariants(TEST_WORD);

  expect(response).toEqual(["robe", "ober", "bert", "rober", "obert"]);
});

test("Telephone Number Convertor - calculate sub variants by size", async () => {
  const response = calcSubVariantsBySize(TEST_WORD, 5);

  expect(response).toEqual(["rober", "obert"]);
});
