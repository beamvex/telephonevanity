import type { ConnectContactFlowEvent, Context } from "aws-lambda";
import { handler } from "../resources/telephone";

const TEST_RESPONSE = {
  caller: "+ 4 4 7 7 7 1 7 1 5 3 0 8",
};

const TEST_EVENT: ConnectContactFlowEvent = {
  Details: {
    ContactData: {
      Attributes: {},
      Channel: "VOICE",
      ContactId: "",
      CustomerEndpoint: {
        Address: "+447771715308",
        Type: "TELEPHONE_NUMBER",
      },
      InitialContactId: "",
      InitiationMethod: "INBOUND",
      InstanceARN: "",
      PreviousContactId: "",
      Queue: null,
      SystemEndpoint: null,
      MediaStreams: {
        Customer: {
          Audio: null,
        },
      },
    },
    Parameters: {},
  },
  Name: "ContactFlowEvent",
};

const TEST_CONTEXT: Context = {
  callbackWaitsForEmptyEventLoop: false,
  functionName: "",
  functionVersion: "",
  invokedFunctionArn: "",
  memoryLimitInMB: "",
  awsRequestId: "",
  logGroupName: "",
  logStreamName: "",
  getRemainingTimeInMillis: function (): number {
    throw new Error("Function not implemented.");
  },
  done: function (error?: Error | undefined, result?: any): void {
    if (error !== undefined) {
      throw new Error(error.message);
    }
    throw new Error("Function not implemented.");
  },
  fail: function (error: string | Error): void {
    throw new Error(typeof error === "string" ? error : error.message);
  },
  succeed: function (messageOrObject: any): void {
    throw new Error("Function not implemented.");
  },
};

test("Telephone Service Lambda Handler", async () => {
  const response = await handler(TEST_EVENT, TEST_CONTEXT, () => {});

  expect(response).toMatchObject(TEST_RESPONSE);
});
