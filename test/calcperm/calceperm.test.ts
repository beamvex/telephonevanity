import { calcPermulations } from "../../resources/calcperm";

const TEST_TELEPHONE_NUMBER = "715308";

test("Number Permutations", async () => {
  const result = calcPermulations(TEST_TELEPHONE_NUMBER);
  expect(result.length).toEqual(108);
});
